<?php

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\CourseCategory;
use App\Entity\CourseLevel;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CourseFixtures extends Fixture implements DependentFixtureInterface
{
    private array $prices = [50, 120, 150, 200, 250, 300, 340.50, 400];
    private array $durations = [60, 120, 180, 220, 300, 360, 500, 600];

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $slugify = new Slugify();
        $categories = $manager->getRepository(CourseCategory::class)->findAll();
        $levels = $manager->getRepository(CourseLevel::class)->findAll();
        $nbPrices = count($this->prices);
        $nbDurations = count($this->durations);
        $nbCat = count($categories);
        $nbLevels = count($levels);
        for($i = 1; $i <= 26; $i++) {
            $course = new Course();
            $course->setCategory($categories[$faker->numberBetween(0, $nbCat -1)]);
            $course->setLevel($levels[$faker->numberBetween(0, $nbLevels -1)]);
            $course->setName($faker->sentence(2, true));
            $course->setSmallDescription($faker->paragraph(1, true));
            $course->setFullDescription($faker->paragraph(5, true));
            $course->setDuration($this->durations[$faker->numberBetween(0, $nbDurations -1)]);
            $course->setPrice($this->prices[$faker->numberBetween(0, $nbPrices -1)]);
            $course->setCreatedAt(new \DateTimeImmutable());
            $course->setIsPublished($faker->boolean(90));
            $course->setSlug($slugify->slugify($course->getName()));
            $course->setImage($i .'.jpg');
            $course->setSchedule($faker->dayOfWeek);
            $course->setProgram($i .'.pdf');
            $manager->persist($course);
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            CourseCategoryFixtures::class,
            CourseLabelFixtures::class,
            ];
    }
}
