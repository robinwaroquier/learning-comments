<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/profile', name: 'profile')]
    public function profile(): Response
    {
        return $this->render('user/profile.html.twig');
    }

    #[Route('/edit-profile', name: 'edit_profile')]
    public function editProfile(): Response
    {
        return $this->render('user/edit-profile.html.twig');
    }

    #[Route('/edit-password', name: 'edit_password')]
    public function editPassword(): Response
    {
        return $this->render('user/edit-password.html.twig');
    }
}
