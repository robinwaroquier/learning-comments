<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Course;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use App\Repository\CourseCategoryRepository;
use App\Repository\CourseRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CourseController extends AbstractController
{

    /**
     * @param CourseRepository $courseRepository
     * @param CourseCategoryRepository $categoryRepository
     * @return Response
     */
    #[Route('/courses', name: 'courses')]
    public function courses(CourseRepository $courseRepository, CourseCategoryRepository $categoryRepository): Response
    {
        $courses = $courseRepository->findBy(
            ['isPublished' => true],
            ['name' => 'ASC']
        );
        $categories = $categoryRepository->findBy(
            [],
            ['name' => 'ASC']
        );
        return $this->render('course/courses.html.twig', [
            'courses'       => $courses,
            'categories'    => $categories
        ]);
    }


    /**
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    #[Route('/course/{slug}', name: 'course')]
    public function course(Course $course, EntityManagerInterface $manager, Request $request): Response
    {
        $comment = new Comment;
        $comment_form = $this->createForm(CommentType::class, $comment);
        $comment_form->handleRequest($request);
        if($comment_form->isSubmitted() && $comment_form->isValid()) {
            $comment->setAuthor($this->getUser());
            $comment->setCourse($course);
            $comment->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($comment);
            $manager->flush();
            $this->addFlash(
                'success',
                'Votre commentaire a été posté!'
            );

            return $this->redirectToRoute('course', ['slug' => $course->getSlug()]);
        }

        return $this->render('course/course.html.twig',
            [
                'course' => $course,
                'commentForm' => $comment_form->createView()
            ]);
    }
}